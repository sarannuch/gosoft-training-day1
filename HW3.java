class HW3 {
    public static void main(String args[]) {
        // draw9(2);
        // draw9(3);
        // draw9(4);
        // draw10(2);
        // draw10(3);
        // draw10(4);
        // draw11(2);
        // draw11(3);
        // draw11(4);
        // draw12(2);
        // draw12(3);
        // draw12(4);
        // draw13(2);
        // draw13(3);
        // draw13(4);
        draw17(2);
        draw17(3);
        draw17(4);
    }

    public static void draw9(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(i * 2);
        }
        System.out.println();
    }

    public static void draw10(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.println(i * 2);
        }
        System.out.println();
    }

    public static void draw11(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j);
            }
            System.out.println();
        }
    }

    public static void draw12(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i == j)
                    System.out.print("-");
                else
                    System.out.print("*");
            }
            System.out.println("");
        }
    }

    public static void draw13(int n) {
        for (int i = n; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {
                if (i == j)
                    System.out.print("-");
                else
                    System.out.print("*");
            }
            System.out.println("");
        }
    }

    public static void draw14(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw15(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = n; j >= 1; j--) {
                if (i <= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw16(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

        for (int i = n - 1; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {

                if (i >= j)
                    System.out.print("*");
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }

    public static void draw17(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (i >= j)
                    System.out.print(i);
                else
                    System.out.print("-");
            }
            System.out.println("");
        }

        for (int i = n - 1; i >= 1; i--) {
            for (int j = 1; j <= n; j++) {

                if (i >= j)
                    System.out.print(i);
                else
                    System.out.print("-");
            }
            System.out.println("");
        }
    }
}
