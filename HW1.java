class HW1 {
    public static void main(String args[]) {
        // draw1(2);
        // draw1(3);
        // draw1(4);
        // draw2(2);
        // draw2(3);
        // draw2(4);
        // draw3(2);
        // draw3(3);
        // draw3(4);
        // draw4(2);
        // draw4(3);
        // draw4(4);
        // draw5(2);
        // draw5(3);
        // draw5(4);
        draw8(2);
        draw8(3);
        draw8(4);
    }

    public static void draw1(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    public static void draw2(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void draw3(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }

    public static void draw4(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = n; j >= 1; j--) {
                System.out.print(j);
            }
            System.out.println();
        }
    }

    public static void draw5(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= n; j++) {
                System.out.print(i);
            }
            System.out.println();
        }
    }

    public static void draw6(int n) {
        for (int i = n; i >= 1; i--) {
            for (int j = n; j >= 1; j--) {
                System.out.print(i);
            }
            System.out.println();
        }
    }

    public static void draw7(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 1; j <= n; j++) {
                System.out.print((i * n) + j);
            }
            System.out.println();
        }
    }

    public static void draw8(int n) {
        for (int i = n-1; i >= 0; i--) {
            for (int j = n; j >= 1; j--) {
                System.out.print((i * n) + j + " ");
            }
            System.out.println();
        }
    }


}
